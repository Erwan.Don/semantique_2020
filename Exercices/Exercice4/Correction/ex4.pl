% Exercice 4.1

% Exemple:
% arbre(node(node(feuille,feuille),feuille)).
% arbre(node(node(feuille,node(feuille,feuille)),feuille)).

arbre(feuille).
arbre(node(G,D)) :- arbre(G), arbre(D).

% Exercice 4.2

% Exemple:
% nbNode(arbre(node(node(feuille,feuille),feuille)), R).

nb(feuille, 0).
nb(node(G,D), Res) :- nb(G,R1),
                      nb(D,R2),
                      Res is 1+R1+R2.

nbNode(arbre(Node), Res) :- nb(Node, Res).

% Exercice 4.3

% Exemple:
% hauteur(arbre(node(node(feuille,feuille),feuille)), R).
% hauteur(arbre(node(node(feuille,node(feuille,feuille)),feuille)),R).

max(X,Y,X) :- X >= Y.
max(X,Y,Y) :- Y > X.

h(feuille, 0).
h(node(G,D), Res) :- h(G,R1), h(D,R2), max(R1,R2,R),
                     Res is 1+R.

hauteur(arbre(Node), Res) :- h(Node, Res).

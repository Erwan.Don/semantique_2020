% 3.1

renverser([],[]).
renverser([H|T], Res) :- renverser(T,R), append(R,[H],Res).

langageA(a).
langageB(b).

langageAB(X) :- langageA(X); langageB(X).
langageAB([]).
langageAB([H|T]) :- langageAB(H), langageAB(T).

palyndrome(X) :- langageAB(X), renverser(X,X).

% 3.2

nbA([],0).
nbA([a|T],Res) :- nbA(T,R), Res is R+1.
nbA([b|T],R) :- nbA(T,R).

nbB([],0).
nbB([b|T],Res) :- nbB(T,R), Res is R+1.
nbB([a|T],R) :- nbB(T,R).

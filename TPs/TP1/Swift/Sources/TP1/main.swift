import LogicKit
import LogicKitBuiltins

// Initial Term
// You can add other terms if needed
let de      : Term = .var("de")
let vers    : Term = .var("vers")
let batterie: Term = .var("batterie")
let reste   : Term = .var("reste")
let pieces  : Term = .var("pieces")
let head    : Term = .var("head")
let tail    : Term = .var("tail")

let x       : Term = .var("x")
let y       : Term = .var("y")
let z       : Term = .var("z")

// KnowledgeBase is a collection which contains all the things we know
let kbMino: KnowledgeBase = [
  
  // Complete the facts and rules in this knowledge base
  // HERE
  
]

// We merge your knowledge base with builtins types in LogicKit
// Also, you can use all operation on Nat and List
let kb: KnowledgeBase = kbMino + KnowledgeBase(knowledge: (List.axioms + Nat.axioms))


// Write your examples to test your rules below !
